Task 5. Docker Practise
==========

Simple identicon app based on monsterid from Alice Smirnova.

To run developmenet server:

```bash
 docker-compose -f docker-compose.yml up --build -d
```

To run production server:
```bash
 docker-compose -f docker-compose.prod.yml up --build -d
```

Also you can pass your own environmental variables in .env file.