from flask import Flask, Response, request, render_template, jsonify
import requests
from utilities import getHash
import redis3 as redis

app = Flask(__name__)
cache = redis.StrictRedis(host='redis', port=6379, db=0)
salt = "UNIQUE_SALT"
default_name = 'Joe Bloggs'


@app.route('/', methods=['GET'])
def mainpage():
    return render_template('index.html', name=default_name, name_hash=getHash(salt, default_name))

@app.route('/', methods=['POST'])
def updateimage():
    name = request.form['name']
    if name is None:
        name = default_name

    name_hash = getHash(salt, name)
    src = "/monster/{name}".format(name=name_hash)
    return jsonify(src=src)


@app.route('/monster/<name>')
def get_identicon(name):

    image = cache.get(name)
    if image is None:
        print ("Cache miss", flush=True)
        r = requests.get('http://dnmonster:8080/monster/' + name + '?size=80')
        image = r.content
        cache.set(name, image)

    return Response(image, mimetype='image/png')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
