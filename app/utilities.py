def getHash(salt, name) :
    import hashlib
    salted_name = salt + name
    name_hash = hashlib.sha256(salted_name.encode()).hexdigest()
    return name_hash