FROM python:3.7-alpine
ARG ENV
ENV ENV $ENV
RUN addgroup -S uwsgi && adduser -S -G uwsgi uwsgi
RUN rm -rf /var/cache/apk/* && \
    rm -rf /tmp/*
RUN apk update && apk add gcc clang libc-dev musl-dev linux-headers
WORKDIR /app
COPY app /app
COPY cmd.sh /
RUN export CC=gcc && pip install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 9090 9191
USER uwsgi

CMD ["/cmd.sh"]
